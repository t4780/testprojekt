import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       short anzahl;
       double gesamtBetrag;

       System.out.print("Zu zahlender Betrag (Euro): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       System.out.println("Anzahl der Fahrkarten: ");
       anzahl = tastatur.nextShort();

       // Geldeinwurf
       // -----------
       gesamtBetrag = zuZahlenderBetrag * anzahl;
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < gesamtBetrag)
       {
    	   double e = gesamtBetrag - eingezahlterGesamtbetrag;
    	   System.out.printf("Noch zu zahlen: " + "%.2f", e);System.out.println(" Euro");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
           
       }

       // Fahrscheinausgabe
       // -----------------
       if(anzahl > 1.9) {System.out.println("\nFahrscheine werden ausgegeben");}
       else {System.out.println("\nFahrschein wird ausgegeben");}
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " Euro");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
    	   
    	   while(rückgabebetrag >=30) {System.out.println("30 Euro Schein"); rückgabebetrag -= 30.0;}

           while(rückgabebetrag >= 2.0) // 2 Euro-Münzen
           {
        	  System.out.println("2 Euro");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 Euro-Münzen
           {
        	  System.out.println("1 Euro");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.048)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }

       }

       System.out.println("\nVergessen Sie den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen keine gute Fahrt.");
    }
}
